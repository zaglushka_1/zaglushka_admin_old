<?php

namespace Modules\Rin\Enum;

use App\Enum\Subjects as baseSubjects;

class Subjects extends baseSubjects
{
    /**
     * @param $subject
     *
     * @return string
     */
    public static function getSubjectName($subject) {
        switch ($subject) {
            case static::SUBJECT_ORDER: return 'Заказ';
            case static::SUBJECT_CATEGORY: return 'Категория';
            case static::SUBJECT_TAG: return 'Тег';
            case static::SUBJECT_ITEM: return 'Товар';
            case static::SUBJECT_COLOR: return 'Цвет';
            case static::SUBJECT_CITY: return 'Город';
            case static::SUBJECT_ITEM_HOT: return 'Промо товар';
            case static::SUBJECT_MANAGER: return 'Менеджер';
            case static::SUBJECT_PROPERTY: return 'Свойство';
            case static::SUBJECT_SLUG: return 'Url';
        }
    }
}