<?php

namespace Modules\Rin\Traits;

use Illuminate\Validation\ValidationException;
use Validator;

trait RinModel
{
    private $validator;

    public function validate()
    {
        $messages = [
            'max' => 'validation.max::max',
            'min' => 'validation.min::min',
        ];

        $Validator       = Validator::make($this->getAttributes(), $this->rules(), $messages);
        $this->validator = $Validator;

        if (!$Validator->passes()) {
            throw new ValidationException($Validator);
        }
    }
}
