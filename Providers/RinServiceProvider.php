<?php

namespace Modules\Rin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Log;
use Modules\Rin\Http\Middleware\IsAdmin;
use Modules\Rin\Http\Middleware\isContent;
use Modules\Rin\Http\Middleware\IsHaveRinAccess;
use Modules\Rin\Http\Middleware\isManager;
use Modules\Rin\Http\Middleware\isSeo;
use Modules\Rin\Http\Middleware\RequestFormatter;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

class RinServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->registerMiddleware();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
/*        $Monolog = Log::getMonolog();
        $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/rin.log'))]);
        $handler->setFormatter(new LineFormatter(null, null, true, true));*/
    }

    /**
     * Register middleware.
     *
     * @return void
     */
    public function registerMiddleware()
    {
        $this->app['router']->aliasMiddleware('admin', IsAdmin::class);
        $this->app['router']->aliasMiddleware('content', IsContent::class);
        $this->app['router']->aliasMiddleware('seo', IsSeo::class);
        $this->app['router']->aliasMiddleware('manager', IsManager::class);
        $this->app['router']->aliasMiddleware('rin_access', IsHaveRinAccess::class);
        $this->app['router']->aliasMiddleware('request_formatter', RequestFormatter::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('rin.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'rin'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/rin');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath,
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/rin';
        }, \Config::get('view.paths')), [$sourcePath]), 'rin');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/rin');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'rin');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'rin');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
