<?php

namespace Modules\Rin\Models;

use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ItemSame
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $same_item_id
 */
class ItemSame extends \App\Models\ItemSame
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'same_item_id'];

    private function rules()
    {
        return [
            'item_id'      => 'required|exists:items,id',
            'same_item_id' => 'required|exists:items,id',
        ];
    }
}
