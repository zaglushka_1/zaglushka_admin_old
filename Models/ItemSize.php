<?php

namespace Modules\Rin\Models;

use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;

class ItemSize extends \App\Models\ItemSize
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['size_1', 'size_2', 'size_3', 'size_4', 'size_5', 'size_6', 'thread_grade'];

    private function rules()
    {
        return [
            'item_id' => 'required|exists:items,id',
            'type'    => 'required|in:' . implode(',', array_keys(static::$types)),
            'size_1'  => 'max:100',
            'size_2'  => 'max:100',
            'size_3'  => 'max:100',
            'size_4'  => 'max:100',
            'size_5'  => 'max:100',
            'size_6'  => 'max:100',
            'thread_grade' => 'max:100'
        ];
    }
}
