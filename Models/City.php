<?php

namespace Modules\Rin\Models;

use Cache;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Modules\Rin\Models\City
 */
class City extends \App\Models\City
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['title', 'post_index', 'free_delivery_price', 'phone'];

    /**
     * @inheritdoc
     */
    public function save(array $options = [])
    {
        if ($this->exists) {
            Cache::delete('city_' . $this->id);
        }

        return parent::save($options);
    }
}
