<?php

namespace Modules\Rin\Models;

use Cache;
use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Validation\Rule;

class Tag extends \App\Models\Tag
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'title_h1',
        'top_level',
        'title_category',
        'descr',
        'ord',
    ];

    private function rules()
    {
        return [
            'title'     => ['required', 'min:1', 'max:250', Rule::unique('tags', 'title')->ignore($this->id)],
            'title_h1'  => 'max:250',
            'top_level' => 'required|boolean',
            'title_category'    =>  'max:255',
            'ord'       => 'integer',
        ];
    }

    /**
     * @inheritdoc
     */
    public function save(array $options = [])
    {
        Cache::delete('tag_' . $this->id);

        $redis = Cache::getRedis();
        $keys1 = $redis->keys(Cache::getPrefix() . 'top_tags_*');
        $keys2 = $redis->keys(Cache::getPrefix() . 'low_tags_*');

        foreach (array_merge($keys1, $keys2) as $key) {
            $redis->del($key);
        }

        return parent::save($options);
    }
}
