<?php

namespace Modules\Rin\Models;

use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Manager
 *
 * @property integer $id
 * @property string $title
 * @property string $phone
 * @property string $email
 * @property boolean $enabled
 * @property integer $ord
 */
class Manager extends \App\Models\Manager
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['title', 'phone', 'email', 'enabled', 'ord'];
}
