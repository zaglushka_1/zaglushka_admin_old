<?php

namespace Modules\Rin\Models;

use Cache;
use Modules\Rin\Enum\Subjects;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int     $id
 * @property int     $subject_id
 * @property int     $object_id
 * @property string  $slug
 * @property boolean $current
 * @property string  $created_at
 * @property string  $updated_at
 */
class Slug extends \App\Models\Slug
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['subject_id', 'object_id', 'slug', 'current'];

    /**
     * @inheritdoc
     */
    public function save(array $options = [])
    {
        if (parent::save($options)) {
            if ($this->exists) {
                if ($this->isDirty('slug')) {
                    Cache::delete('slug_by_string_' . $this->getOriginal('slug') . '_0');
                    Cache::delete('slug_by_string_' . $this->getOriginal('slug') . '_' . Subjects::SUBJECT_CATEGORY);
                    Cache::delete('slug_by_string_' . $this->getOriginal('slug') . '_' . Subjects::SUBJECT_TAG);
                }

                Cache::delete('slug_by_id_' . $this->subject_id . '_' . $this->object_id);
            }
        } else {
            return false;
        }

        return true;
    }

    public function delete()
    {
        if (parent::delete()) {
            Cache::delete('slug_by_string_' . $this->slug . '_0');
            Cache::delete('slug_by_string_' . $this->slug . '_' . Subjects::SUBJECT_CATEGORY);
            Cache::delete('slug_by_string_' . $this->slug . '_' . Subjects::SUBJECT_TAG);

            Cache::delete('slug_by_id_' . $this->subject_id . '_' . $this->object_id);
        } else {
            return false;
        }

        return true;
    }
}
