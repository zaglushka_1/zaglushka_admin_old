<?php

namespace Modules\Rin\Models;

use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ItemHasTag
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $tag_id
 */
class ItemHasTag extends \App\Models\ItemHasTag
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    private function rules()
    {
        return [
            'item_id'   => 'required|exists:items,id',
            'tag_id'   => 'required|exists:tags,id',
        ];
    }
}
