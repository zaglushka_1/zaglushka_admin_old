<?php

namespace Modules\Rin\Models;

use Illuminate\Validation\Rule;
use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;

class ItemColor extends \App\Models\ItemColor
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['code', 'main', 'add_price', 'prop_features','ord', 'color'];

    private function rules()
    {
        return [
            'code'      => Rule::unique('items_colors'),
            'item_id'   => 'required|exists:items,id',
            'prop_features' => 'nullable|integer',
            'main'      => 'required|boolean',
            'add_price' => 'required|numeric:' . implode(',', array_keys(static::$additionalPrices)),
            'ord'       => 'required|integer',
            'color'     => 'required|in:' . implode(',', Color::pluck('id')->toArray()),
        ];
    }
}
