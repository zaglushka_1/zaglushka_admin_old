<?php

namespace Modules\Rin\Models;

use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $item_id
 * @property int $tag_id
 * @property Item $item
 * @property Tag $tag
 */
class ItemHasAddTag extends \App\Models\ItemHasAddTag
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    private function rules()
    {
        return [
            'item_id'   => 'required|exists:items,id',
            'tag_id'   => 'required|exists:tags,id',
        ];
    }
}
