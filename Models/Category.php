<?php

namespace Modules\Rin\Models;

use Cache;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends \App\Models\Category
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'title',
        'ord',
        'descr',
        'type',
        'enabled',
        'title_h1',
        'exclude_top_tags',
        'hide_price',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return '/images/cat/';
    }

    /**
     * @return array
     */
    public function getImages()
    {
        $Images = [
            static::IMAGE_PATH_LIST => $this->isImageExists(self::IMAGE_PATH_LIST) ? $this->getImagePath(self::IMAGE_PATH_LIST) : null,
            static::IMAGE_PATH_MENU => $this->isImageExists(self::IMAGE_PATH_MENU) ? $this->getImagePath(self::IMAGE_PATH_MENU) : null,
        ];

        return $Images;
    }

    /**
     * @return array
     */
    public function getImagesWithUrl()
    {
        $Images = [];

        foreach ($this->getImages() as $key => $value) {
            $Images[$key] = getenv('APP_URL') . $value;
        }

        return $Images;
    }

    /**
     * @param string $sort
     *
     * @return Item
     */
    public function items($sort = null) : Builder
    {
        $Items = Item::where(function ($query) {
            $query->where('cat_id', $this->id)
                ->orWhere('add_cat_id', $this->id);
        });

        $Items = $Items
            ->orderByRaw('-enabled ASC')
            ->orderBy('enabled', 'DESC');

        if ($sort) {
            $Items = $Items
                ->orderByRaw('-' . $sort . ' DESC')
                ->orderBy($sort, 'ASC');
        }

        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        return $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');
    }

    /**
     * @inheritdoc
     */
    public function save(array $options = [])
    {
        if (!$this->exists || $this->isDirty('parent_id')) {
            Cache::delete('subcategories_ids_' . $this->parent_id);
            Cache::delete('subcategories_ids_' . $this->getOriginal('parent_id'));
        }

        if (!$this->parent_id) {
            Cache::delete('topcategories');
        }

        Cache::delete('category_' . $this->id);

        return parent::save($options);
    }
}
