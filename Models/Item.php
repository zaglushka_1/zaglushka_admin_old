<?php

namespace Modules\Rin\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Validation\Rule;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Slug as ModelSlug;
use Modules\Rin\Traits\RinModel;
use Slug;
use Spatie\Activitylog\Traits\LogsActivity; 
use App\Models\ProductionItems;
use App\Models\Production;

class Item extends \App\Models\Item
{
    use RinModel;

    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = [
        'cat_id',
        'add_cat_id',
        'code',
        'title',
        'field_h1',
        'price0',
        'price1',
        'price2',
        'price3',
        'price4',
        'price5',
        'price6',
        'add_price0',
        'add_price1',
        'add_price2',
        'add_price3',
        'add_price4',
        'add_price5',
        'add_price6',
        'pack_size',
        'descr',
        'title_smart',
        'description_smart',
        'weight',
        'volume',
        'production',
        'short_descr',
        'prop_matherial',
        'prop_package',
        'prop_pipes',
        'prop_weight',
        'prop_pack_width',
        'prop_pack_length',
        'prop_pack_height',
        'prop_pack_quant',
        'prop_pellet_width',
        'prop_pellet_length',
        'prop_pellet_height',
        'prop_pellet_quant_pack',
        'prop_pack_weight',
        'enabled',
        'under_title',
        'under_title_color',
        'special',
        'special_price',
        'prop_pack_weight_gross',
        'prop_pack_weight_net',
        'prop_pack_volume',
        'prop_fit_pipe',
        'prop_futures',
//        'logo_image',
//        'logo_tab_content',
        'no_free_delivery',
        'custom_search_position',
        'custom_cat_position',
        'custom_tag_position',
        'yandex_name',
        'recommend',
        'two_price',
        'hide_remains',
        'price_main',
        'preorder_availability_days_min',
        'preorder_availability_days_max',
        'width',
        'length',
        'height',
        'promo',
        'name',
        'only_two_prices',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    private function rules()
    {
        return [
            'cat_id'      => 'required|exists:cats,id',
            'add_cat_id'  => 'nullable|exists:cats,id',
            'code'        => 'required|min:1|max:255',
            'title'       => ['required', 'min:1', 'max:255', Rule::unique('items', 'title')->ignore($this->id)],
            'title_smart'       => ['min:1', 'max:255'],
            'field_h1'  => 'nullable|string|max:255',
            'under_title' => 'nullable|string|max:255',
            'price0'      => 'nullable|numeric',
            'price1'      => 'nullable|numeric',
            'price2'      => 'nullable|numeric',
            'price3'      => 'nullable|numeric',
            'price4'      => 'nullable|numeric',
            'price5'      => 'nullable|numeric',
            'price6'      => 'nullable|numeric',
            'add_price0'      => 'nullable|numeric',
            'add_price1'      => 'nullable|numeric',
            'add_price2'      => 'nullable|numeric',
            'add_price3'      => 'nullable|numeric',
            'add_price4'      => 'nullable|numeric',
            'add_price5'      => 'nullable|numeric',
            'add_price6'      => 'nullable|numeric',
            'pack_size'   => 'nullable|integer',
            'descr'       => 'required|string',
            'weight'      => 'nullable|numeric',
            'volume'      => 'nullable|numeric',
            'special_price' => 'nullable|numeric',
            'production'  => 'nullable|string|max:255',
            'short_descr' => 'required|string',

            'enabled' => 'boolean',

            'under_title' => 'nullable|string|max:255',
            'special'     => 'boolean',

            'prop_matherial'         => 'nullable|integer',
            'prop_package'           => 'nullable|numeric',
            'prop_pipes'             => 'nullable|string|max:255',
            'prop_weight'            => 'nullable|numeric',
            'prop_pack_width'        => 'nullable|integer',
            'prop_pack_length'       => 'nullable|integer',
            'prop_pack_height'       => 'nullable|integer',
            'prop_pack_quant'        => 'nullable|integer',
            'prop_pellet_width'      => 'nullable|integer',
            'prop_pellet_length'     => 'nullable|integer',
            'prop_pellet_height'     => 'nullable|integer',
            'prop_pellet_quant_pack' => 'nullable|integer',

            'prop_pack_weight'       => 'nullable|numeric',
            'prop_pack_weight_gross' => 'nullable|numeric',
            'prop_pack_weight_net'   => 'nullable|numeric',
            'prop_pack_volume'       => 'nullable|numeric',

            'prop_fit_pipe' => 'nullable|integer',


//            'logo_image'                     => 'boolean',
//            'logo_tab_content'               => 'nullable|string',

            'no_free_delivery' => 'boolean',

            'custom_search_position' => 'nullable|integer',
            'custom_cat_position'    => 'nullable|integer',
            'custom_tag_position'    => 'nullable|integer',

            'yandex_name' => 'required|string|max:120',
            'recommend'   => 'boolean',
            'two_price'   => 'boolean',
            'hide_remains'=> 'boolean',
            'price_main'=> 'boolean',


            'width'  => 'nullable|numeric',
            'length' => 'nullable|numeric',
            'height' => 'nullable|numeric',

            'preorder_availability_days_min' => 'nullable|integer',
            'preorder_availability_days_max' => 'nullable|integer',

            'promo' => 'boolean',
            'only_two_prices' => 'boolean',
        ];
    }

    /**
     * @return string
     */
    public function getImageFolder()
    {
        return '/images/item/' . ($this->id % 100) . '/' . $this->id . '/';
    }

    /**
     * @return array
     */
    public function getImages() : array
    {
        $imageTypes = [
            static::IMAGE_TYPE_1,
            static::IMAGE_TYPE_2,
            static::IMAGE_TYPE_3,
            static::IMAGE_TYPE_4,
            static::IMAGE_TYPE_5,
            static::IMAGE_TYPE_6,
            static::IMAGE_TYPE_7,
            static::IMAGE_TYPE_8,
            static::IMAGE_TYPE_9,
            static::IMAGE_TYPE_10,
            static::IMAGE_TYPE_11,
            static::IMAGE_TYPE_12,
            static::IMAGE_TYPE_13,
            static::IMAGE_TYPE_14,
            static::IMAGE_TYPE_15,
            static::IMAGE_TYPE_16,
            static::IMAGE_TYPE_17,
            static::IMAGE_TYPE_18,
            static::IMAGE_TYPE_19,
            static::IMAGE_TYPE_20,
            static::IMAGE_TYPE_21,
            static::IMAGE_TYPE_22,
            static::IMAGE_TYPE_23,
            static::IMAGE_TYPE_24,
            static::IMAGE_TYPE_25,
            static::IMAGE_TYPE_26,
            static::IMAGE_TYPE_27,
            static::IMAGE_TYPE_28,
            static::IMAGE_TYPE_29,
            static::IMAGE_TYPE_30,
            static::IMAGE_TYPE_31,
            static::IMAGE_TYPE_32,
            static::IMAGE_TYPE_33,
            static::IMAGE_TYPE_34,
        ];

        $Images = [];

        foreach ($imageTypes as $image) {
            if ($this->isImageExists($image)) {
                $Images[$image] = parent::getImagePath($image);
            }
        }

        return $Images;
    }

    /**
     * @return array
     */
    public function getImagesWithUrl()
    {
        $Images = [];

        foreach ($this->getImages() as $key => $value) {
            $Images[$key] = getenv('APP_URL') . $value;
        }

        return $Images;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return route('slug', ['slug' => $this->slug()]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itemSizes() : HasMany
    {
        return $this->hasMany(ItemSize::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itemColors() : HasMany
    {
        return $this->hasMany(ItemColor::class)->orderBy('ord', 'ASC');
    }

    /**
     *
     */
    public function generateSlug()
    {
        $Slug             = new ModelSlug();
        $Slug->current    = true;
        $Slug->slug       = str_slug(Slug::make(($this->yandex_name ? $this->yandex_name . '-' : '') . $this->title));
        $Slug->subject_id = Subjects::SUBJECT_ITEM;
        $Slug->object_id  = $this->id;

        return $Slug->saveOrFail();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() : BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'items_tags');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addTags() : BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'items_add_tags');
    }

    /**
     * @inheritdoc
     */
    public function fill(array $attributes)
    {
        parent::fill($attributes);

        $this->under_title                    = $this->under_title ?: '';
        $this->logo_tab_content               = $this->logo_tab_content ?: '';
        $this->prop_pipes                     = $this->prop_pipes ?: '';
        $this->production                     = $this->production ?: '';
        $this->yandex_name                    = $this->yandex_name ?: '';
        $this->field_h1                       = $this->field_h1 ?: '';
        $this->price0                         = $this->price0 ?: null;
        $this->price1                         = $this->price1 ?: null;
        $this->price2                         = $this->price2 ?: null;
        $this->price3                         = $this->price3 ?: null;
        $this->price4                         = $this->price4 ?: null;
        $this->price5                         = $this->price5 ?: null;
        $this->price6                         = $this->price6 ?: null;
        $this->add_price0                         = $this->add_price0 ?: null;
        $this->add_price1                         = $this->add_price1 ?: null;
        $this->add_price2                         = $this->add_price2 ?: null;
        $this->add_price3                         = $this->add_price3 ?: null;
        $this->add_price4                         = $this->add_price4 ?: null;
        $this->add_price5                         = $this->add_price5 ?: null;
        $this->add_price6                         = $this->add_price6 ?: null;
        $this->special_price                  = $this->special_price ?: null;
        $this->pack_size                      = $this->pack_size ?: 0;
        $this->preorder_availability_days_min = $this->preorder_availability_days_min ?: 0;
        $this->preorder_availability_days_max = $this->preorder_availability_days_max ?: 0;
        

        return $this;
    }
}
