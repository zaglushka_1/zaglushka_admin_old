<?php

namespace Modules\Rin\Models;

use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ItemComplex
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $complect_item_id
 */
class ItemComplex extends \App\Models\ItemComplex
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
}
