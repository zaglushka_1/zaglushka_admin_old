import Space from '@/components/Space'

import SeoItems from '@/views/seo/items/Index'
import SeoItemsDefault from '@/views/seo/items/Default'
import SeoItemsList from '@/views/seo/items/List'
import SeoItemsSublist from '@/views/seo/items/Sublist'

import SeoCategoriesList from '@/views/seo/categories/List'
import SeoCategoriesDefault from '@/views/seo/categories/Default'
import SeoCategories from '@/views/seo/categories/Index'

import tags from './tags'

export default {
  path: '/seo',
  component: Space,
  children: [
    tags,
    {
      path: 'items',
      component: Space,
      children: [
        {
          path: '/',
          name: 'seo-items-list',
          component: SeoItemsList,
          meta: {
            title: 'Список товаров SEO',
            permission: 'read seo-items'
          }
        },
        {
          path: 'default',
          name: 'seo-items-default',
          component: SeoItemsDefault,
          meta: {
            title: 'Редактирование SEO товара по умолчанию',
            permission: 'read seo-items'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'seo-items-sublist',
          component: SeoItemsSublist,
          meta: {
            title: 'Список товаров SEO',
            permission: 'read seo-items'
          }
        },
        {
          path: 'item/:id(\\d+)',
          name: 'seo-items',
          component: SeoItems,
          meta: {
            title: 'Редактирование SEO товара',
            permission: 'read seo-items'
          }
        }
      ]
    },
    {
      path: 'categories',
      component: Space,
      children: [
        {
          path: '/',
          name: 'seo-categories-list',
          component: SeoCategoriesList,
          meta: {
            permission: 'read seo-categories',
            title: 'Список категорий'
          }
        },
        {
          path: 'default',
          name: 'seo-categories-default',
          component: SeoCategoriesDefault,
          meta: {
            title: 'Редактирование SEO категории по умолчанию',
            permission: 'read seo-categories'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'seo-categories',
          component: SeoCategories,
          meta: {
            title: 'Редактирование SEO категории',
            permission: 'read seo-categories'
          }
        }
      ]
    }
  ]
}
