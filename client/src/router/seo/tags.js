import Space from '@/components/Space'

import SeoTagsTop from '@/views/seo/tags/Top'
import SeoTagsUnder from '@/views/seo/tags/Under'
import SeoTagsSeo from '@/views/seo/tags/Seo'
import SeoTagsDefault from '@/views/seo/tags/Default'
import SeoTags from '@/views/seo/tags/Index'

export default {
  path: 'tags',
  component: Space,
  children: [
    {
      path: 'top',
      component: Space,
      children: [
        {
          path: '/',
          name: 'seo-tags-top-list',
          component: SeoTagsTop,
          meta: {
            title: 'SEO / Список верхних тегов',
            permission: 'read seo-tags'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'seo-tags-top',
          component: SeoTags,
          meta: {
            title: 'Редактирование SEO тега',
            permission: 'read seo-tags'
          }
        }
      ]
    },
    {
      path: 'under',
      component: Space,
      children: [
        {
          path: '/',
          name: 'seo-tags-under-list',
          component: SeoTagsUnder,
          meta: {
            title: 'SEO / Список нижних тегов',
            permission: 'read seo-tags'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'seo-tags-under',
          component: SeoTags,
          meta: {
            title: 'Редактирование SEO тега',
            permission: 'read seo-tags'
          }
        }
      ]
    },
    {
      path: 'seotags',
      component: Space,
      children: [
        {
          path: '/',
          name: 'seo-tags-seo-list',
          component: SeoTagsSeo,
          meta: {
            title: 'SEO / Список SEO тегов',
            permission: 'read seo-tags'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'seo-tags-seo',
          component: SeoTags,
          meta: {
            title: 'Редактирование SEO тега',
            permission: 'read seo-tags'
          }
        }
      ]
    },
    {
      path: 'default',
      name: 'seo-tags-default',
      component: SeoTagsDefault,
      meta: {
        title: 'Редактирование SEO тега по умолчанию',
        permission: 'read seo-tags'
      }
    }
  ]
}
