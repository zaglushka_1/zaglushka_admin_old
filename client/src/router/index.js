import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Layout from '@/components/Layout'

import NotFoundComponent from '@/views/NotFoundComponent'

import Login from '@/views/Login'
import Hello from '@/views/Hello'
import Profile from '@/views/Profile'
import Settings from '@/views/Settings'

import admin from '@/router/admin'
import seo from '@/router/seo'
import sales from '@/router/sales'
import content from '@/router/content'

Vue.use(Router)

const router = new Router({
  base: 'rin',
  mode: 'history',
  scrollBehavior (savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: 'Авторизация',
        permission: 'any'
      },
      beforeEnter: function (to, from, next) {
        if (!store.state.user.isLoggedIn) {
          store.dispatch('user/check').then(() => {
            next('/')
          }, () => {
            next()
          })
        } else {
          next('/')
        }
      }
    },
    {
      path: '/logout',
      name: 'logout',
      meta: {
        permission: 'any'
      },
      beforeEnter: function (to, from, next) {
        store.dispatch('user/logout').then(() => {
          next('/login')
        })
      }
    },
    {
      path: '/',
      component: Layout,
      children: [
        {
          path: '/',
          redirect: {
            name: 'dashboard'
          }
        },
        admin,
        seo,
        content,
        sales,
        {
          path: '/profile',
          name: 'profile',
          component: Profile,
          meta: {
            title: 'Профиль',
            permission: 'any',
            error: '/not-found'
          }
        },
        {
          path: '/settings',
          name: 'settings',
          component: Settings,
          meta: {
            title: 'Настройки',
            permission: 'read settings',
            error: '/not-found'
          }
        },
        {
          path: '/dashboard',
          name: 'dashboard',
          component: Hello,
          meta: {
            title: 'Панель администратора',
            permission: 'any'
          }
        },
        {
          path: '*',
          name: 'not-found',
          meta: {
            title: 'Страница не найдена',
            permission: 'any'
          },
          component: NotFoundComponent
        },
        {
          path: '/not-found',
          meta: {
            title: 'Страница не найдена',
            permission: 'any'
          },
          component: NotFoundComponent
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  store.dispatch('pageload/complete')
  if (!store.state.user.isLoggedIn && to.name !== 'login') {
    store.dispatch('user/check').then(() => {
      next()
    }, () => {
      let path = '/'

      if (to.fullPath) {
        path = to.fullPath
      }

      next({name: 'login', query: {path}})
    })
  } else {
    next()
  }

  document.title = to.meta.title
})

export default router
