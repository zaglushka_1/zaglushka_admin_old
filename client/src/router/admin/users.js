import Space from '@/components/Space'

import AdminUsersList from '@/views/admin/users/List'

export default {
  path: 'users',
  component: Space,
  children: [
    {
      path: '/',
      name: 'admin-users-list',
      component: AdminUsersList,
      meta: {
        title: 'Пользователи',
        permission: 'read users'
      }
    }
  ]
}
