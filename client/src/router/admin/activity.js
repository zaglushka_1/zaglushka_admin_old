import Space from '@/components/Space'

import AdminActivityList from '@/views/admin/activity/List'
import AdminActivitySubject from '@/views/admin/activity/Subject'
import AdminActivityUser from '@/views/admin/activity/User'

export default {
  path: 'activity',
  component: Space,
  children: [
    {
      path: '/',
      name: 'admin-activity-list',
      component: AdminActivityList,
      meta: {
        title: 'Активность',
        permission: 'read activity'
      }
    },
    {
      path: 'subject/:subjectId(\\d+)/:objectId(\\d+)',
      name: 'admin-activity-subject',
      component: AdminActivitySubject,
      meta: {
        title: 'Активность',
        permission: 'read activity'
      }
    },
    {
      path: 'user/:id(\\d+)',
      name: 'admin-activity-user',
      component: AdminActivityUser,
      meta: {
        title: 'Активность',
        permission: 'read activity'
      }
    }
  ]
}
