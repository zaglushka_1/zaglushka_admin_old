import Space from '@/components/Space'

import ContentItemsCategories from '@/views/content/items/Categories'
import ContentItemsList from '@/views/content/items/List'
import ContentItems from '@/views/content/items/Index'
import ContentItemsCreate from '@/views/content/items/Create'
import ContentItemsCopy from '@/views/content/items/Copy'

export default {
  path: 'items',
  component: Space,
  children: [
    {
      path: '/',
      name: 'content-items-categories',
      component: ContentItemsCategories,
      meta: {
        title: 'Список "Товаров"',
        permission: 'read items'
      }
    },
    {
      path: 'create',
      name: 'content-items-create',
      component: ContentItemsCreate,
      meta: {
        title: 'Добавление товара',
        permission: 'add items'
      }
    },
    {
      path: 'list/:id(\\d+)',
      name: 'content-items-list',
      component: ContentItemsList,
      meta: {
        title: 'Список "Товаров"',
        permission: 'read items'
      }
    },
    {
      path: ':id(\\d+)',
      name: 'content-items',
      component: ContentItems,
      meta: {
        title: 'Редактирование товара',
        permission: 'read items'
      }
    },
    {
      path: 'copy/:id(\\d+)',
      name: 'content-items-copy',
      component: ContentItemsCopy,
      meta: {
        title: 'Копирование товара',
        permission: 'add items'
      }
    }
  ]
}
