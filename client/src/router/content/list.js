import Space from '@/components/Space'

import ContentListMaterials from '@/views/content/list/Materials'

import ContentListCharacters from '@/views/content/list/Сharacters'

import ContentListFeatures from '@/views/content/list/Features'

import ContentListSuitableForPipes from '@/views/content/list/SuitableForPipes'

import ContentListTypesPackages from '@/views/content/list/TypesPackages'

import ContentListCities from '@/views/content/list/Cities'

import ContentListManagers from '@/views/content/list/Managers'

import ContentListColors from '@/views/content/list/Colors'

import ContentListFactories from '@/views/content/list/Factories'

import ContentListUndertitles from '@/views/content/list/Undertitles'

import ContentListLanguages from '@/views/content/list/Languages'

export default {
  path: 'list',
  component: Space,
  children: [
    {
      path: 'materials',
      name: 'content-list-materials',
      component: ContentListMaterials,
      meta: {
        title: 'Список "Материалов"',
        permission: 'read props'
      }
    },
    {
      path: 'characters',
      name: 'content-list-characters',
      component: ContentListCharacters,
      meta: {
        title: 'Список "Характеристики"',
        permission: 'read props'
      }
    },
    {
      path: 'features',
      name: 'content-list-features',
      component: ContentListFeatures,
      meta: {
        title: 'Список "Особенности монтажа"',
        permission: 'read props'
      }
    },
    {
      path: 'suitable-for-pipes',
      name: 'content-list-suitable-for-pipes',
      component: ContentListSuitableForPipes,
      meta: {
        title: 'Список "Подходит для труб"',
        permission: 'read props'
      }
    },
    {
      path: 'types-packages',
      name: 'content-list-types-packages',
      component: ContentListTypesPackages,
      meta: {
        title: 'Список "Тип упаковки"',
        permission: 'read props'
      }
    },
    {
      path: 'cities',
      name: 'content-list-cities',
      component: ContentListCities,
      meta: {
        title: 'Список "Городов"',
        permission: 'read cities'
      }
    },
    {
      path: 'managers',
      name: 'content-list-managers',
      component: ContentListManagers,
      meta: {
        title: 'Список "Менеджеров"',
        permission: 'read managers'
      }
    },
    {
      path: 'colors',
      name: 'content-list-colors',
      component: ContentListColors,
      meta: {
        title: 'Список "Цветов"',
        permission: 'read colors'
      }
    },
    {
      path: 'factories',
      name: 'content-list-factories',
      component: ContentListFactories,
      meta: {
        title: 'Список "Фабрик"',
        permission: 'read factories'
      }
    },
    {
      path: 'undertitles',
      name: 'content-list-undertitles',
      component: ContentListUndertitles,
      meta: {
        title: 'Список "Артикулов"',
        permission: 'read undertitles'
      }
    },
    {
      path: 'languages',
      name: 'content-list-languages',
      component: ContentListLanguages,
      meta: {
        title: 'Список "Фраз"',
        permission: 'read undertitles'
      }
    }
  ]
}
