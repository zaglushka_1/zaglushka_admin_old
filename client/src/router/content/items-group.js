import Space from '@/components/Space'

import ContentCategories from '@/views/content/items-groups/Index'
import ContentCategoriesList from '@/views/content/items-groups/List'
import ContentCategoriesCreate from '@/views/content/items-groups/Create'

export default {
  path: 'items-group',
  component: Space,
  children: [
    {
      path: '/',
      name: 'content-items-group-list',
      component: ContentCategoriesList,
      meta: {
        title: 'Список "Категорий"',
        permission: 'read categories'
      }
    },
    {
      path: ':id(\\d+)',
      name: 'content-items-group',
      component: ContentCategories,
      meta: {
        title: 'Редактирование категории',
        permission: 'read categories'
      }
    },
    {
      path: 'create',
      name: 'content-items-group-create',
      component: ContentCategoriesCreate,
      meta: {
        title: 'Добавление категории',
        permission: 'add categories'
      }
    }
  ]
}
