import Space from '@/components/Space'

import ContentComplexItemsCategories from '@/views/content/complex-items/Categories'
import ContentComplexItemsList from '@/views/content/complex-items/List'
import ContentComplexItems from '@/views/content/complex-items/Index'

export default {
  path: 'complex-items',
  component: Space,
  children: [
    {
      path: '/',
      name: 'content-complex-items-categories',
      component: ContentComplexItemsCategories,
      meta: {
        title: 'Список "Составных товаров"',
        permission: 'read items'
      }
    },
    {
      path: 'list/:id(\\d+)',
      name: 'content-complex-items-list',
      component: ContentComplexItemsList,
      meta: {
        title: 'Список "Составных Товаров"',
        permission: 'read items'
      }
    },
    {
      path: ':id(\\d+)',
      name: 'content-complex-items',
      component: ContentComplexItems,
      meta: {
        title: 'Редактирование составного товара',
        permission: 'read items'
      }
    }
  ]
}
