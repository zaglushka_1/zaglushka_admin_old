import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.config.productionTip = false

/* eslint-disable no-new */
const vue = new Vue({
  http: {
    root: '/rin/api'
  }
})

export default {
  seoCategories: vue.$resource('seo/categories{/id}'),
  contentCategories: vue.$resource('content/categories{/id}', {}, {
    slugDelete: {
      method: 'DELETE',
      url: 'content/categories{/id}/slug'
    },
    slugCreate: {
      method: 'POST',
      url: 'content/categories{/id}/slug'
    }
  }),
  contentItems: vue.$resource('content/items{/id}', {}, {
    update: {
      method: 'POST'
    },
    new: {
      method: 'GET',
      url: 'content/items/new'
    },
    copy: {
      method: 'POST',
      url: 'content/items/copy{/id}'
    },
    getList: {
      method: 'GET',
      url: 'content/items/list/{subcategoryId}'
    },
    validate: {
      method: 'POST',
      url: 'content/items{/id}'
    },
    imageSwap: {
      method: 'POST',
      url: 'content/items{/id}/image-swap'
    },
    imageMultiChange: {
      method: 'POST',
      url: 'content/items/image-multi-change'
    },
    slugDelete: {
      method: 'DELETE',
      url: 'content/items{/id}/slug'
    },
    slugCreate: {
      method: 'POST',
      url: 'content/items{/id}/slug'
    }
  }),
  auth: vue.$resource('/rin/login'),
  logout: vue.$resource('/rin/logout'),
  user: vue.$resource('user'),
  seoSlugs: vue.$resource('seo/slugs{/subjectId}{/objectId}', {}, {
    delete: {
      method: 'DELETE',
      url: 'seo/slugs{/id}'
    },
    setCurrent: {
      method: 'PUT',
      url: 'seo/slugs{/id}'
    },
    save: {
      method: 'POST',
      url: 'seo/slugs'
    }
  }),
  items: vue.$resource('items{/id}', {}, {
    getList: {
      method: 'GET',
      url: 'items/list/{subcategoryId}'
    }
  }),
  tags: vue.$resource('tags{/id}', {}, {
    getTop: {
      method: 'GET',
      url: 'tags-top'
    },
    getUnder: {
      method: 'GET',
      url: 'tags-under'
    },
    getSeotags: {
      method: 'GET',
      url: 'tags-seo'
    }
  }),
  search: {
    items: vue.$resource('search/items'),
    categories: vue.$resource('search/categories'),
    tags: vue.$resource('search/tags'),
    parentCategories: vue.$resource('search/parent-categories')
  },
  props: vue.$resource('content/props{/type}', {}, {
    update: {
      method: 'PUT',
      url: 'content/props{/id}'
    },
    save: {
      method: 'POST',
      url: 'content/props'
    },
    delete: {
      method: 'DELETE',
      url: 'content/props{/id}'
    }
  }),
  contentCities: vue.$resource('content/cities', {}, {
    update: {
      method: 'PUT',
      url: 'content/cities{/id}'
    },
    delete: {
      method: 'DELETE',
      url: 'content/cities{/id}'
    }
  }),
  contentManagers: vue.$resource('content/managers', {}, {
    update: {
      method: 'PUT',
      url: 'content/managers{/id}'
    },
    delete: {
      method: 'DELETE',
      url: 'content/managers{/id}'
    }
  }),
  contentItemsHot: vue.$resource('content/items-hot{/id}', {}, {
    update: {
      method: 'POST',
      url: 'content/items-hot{/id}'
    },
    save: {
      method: 'POST',
      url: 'content/items-hot'
    },
    validate: {
      method: 'POST',
      url: 'content/items-hot{/id}'
    }
  }),
  contentImport: vue.$resource('content/import'),
  contentBreadcrumbs: vue.$resource('content/breadcrumbs{/categoryId}{/tagId}', {}, {
    getList: {
      method: 'GET',
      url: 'content/breadcrumbs'
    }
  }),
  contentComplex: vue.$resource('content/complex{/id}', {}, {
    getList: {
      method: 'GET',
      url: 'content/complex'
    }
  }),
  seoMeta: vue.$resource('seo/meta'),
  contentColors: vue.$resource('content/colors{/id}', {}, {
    getList: {
      method: 'GET',
      url: 'content/colors'
    }
  }),
  contentTags: vue.$resource('content/tags{/id}', {}, {
    getList: {
      method: 'GET',
      url: 'content/tags'
    },
    order: {
      method: 'POST',
      url: 'content/tags/order'
    }
  }),
  salesOrders: vue.$resource('sales/orders{/id}', {}, {
    getList: {
      method: 'GET',
      url: 'sales/orders'
    }
  }),
  salesOrderSendMessage: vue.$resource('sales/order/send-message{/id}'),
  adminUsers: vue.$resource('admin/users{/id}', {}, {
    getList: {
      method: 'GET',
      url: 'admin/users'
    }
  }),
  exceptions: vue.$resource('exceptions'),
  activity: vue.$resource('activity', {}, {
    getList: {
      method: 'GET',
      url: 'activity/list'
    },
    getBySubject: {
      method: 'GET',
      url: 'activity/subject/{subjectId}{/objectId}'
    },
    getByUser: {
      method: 'GET',
      url: 'activity/user{/id}'
    }
  }),
  contentManufacturers: vue.$resource('content/manufacturers', {}, {
    getList: {
      method: 'GET',
      url: 'content/manufacturers'
    },
    update: {
      method: 'PUT',
      url: 'content/manufacturers'
    }
  }),
  contentUndertitles: vue.$resource('content/undertitles', {}, {
    getList: {
      method: 'GET',
      url: 'content/undertitles'
    }
  }),
  contentI10n: vue.$resource('content/i10n{/id}'),
  settingsSalesEmails: vue.$resource('settings/sales-emails'),
  settings: vue.$resource('settings{/id}')
}
