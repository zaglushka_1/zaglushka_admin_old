import Api from '@/api'

const state = {
  email: false,
  name: false,
  id: false
}

const mutations = {
  login (state, user) {
    if (user.roles && this._vm.access.length < 2) {
      const permissions = user.roles[0].permissions.map(permisson => permisson.name)
      permissions.push('any')

      this._vm.access = permissions
    }
    state.email = user.email
    state.name = user.name
    state.id = user.id
  },
  logout (state) {
    state.user = false
  }
}

const actions = {
  login ({ commit }, creds) {
    return new Promise((resolve, reject) => {
      Api.auth.save(creds).then(response => {
        commit('login', response.body)
        resolve(response)
      }, response => {
        reject(response)
      })
    })
  },
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      Api.logout.save().then(response => {
        commit('logout')
        resolve(response)
      }, response => {
        reject(response)
      })
    })
  },
  check ({ commit }) {
    return new Promise((resolve, reject) => {
      Api.user.get().then(response => {
        commit('login', response.body)
        setTimeout(() => {
          resolve(response)
        })
      }, response => {
        reject(response)
      })
    })
  }
}

const getters = {
  isLoggedIn: state => {
    return state.user && state.user.id
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
