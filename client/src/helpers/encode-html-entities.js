import { AllHtmlEntities } from 'html-entities'

export default value => AllHtmlEntities.encode(value)
