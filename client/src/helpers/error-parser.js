// import Api from '@/api'

// import serializeError from 'serialize-error'

import router from '../router'

export default res => {
  const body = res.body

  if (!body) {
    if (res instanceof Error) {
      /*
      Api.exceptions.save({
        exception: JSON.stringify(serializeError(res))
      })
      */

      return res.message
    }

    return 'Неизвестная ошибка<br/> ¯\\_(ツ)_/¯'
  }

  if (res.status === 401) {
    router.push({ path: 'login' })
  }

  let message = ''

  message += res.status && STATUS_ERRORS[res.status] ? STATUS_ERRORS[res.status] + '<br/><br/>' : ''

  if (!body.errors || !body.message) {
    return message + 'Неизвестная ошибка<br/>' + (body.message ? '<code>' + body.message + '</code>' : '¯\\_(ツ)_/¯')
  }

  const errors = Object.entries(body.errors)

  errors.forEach(([key, value], i) => {
    message += i + 1 !== errors.length ? '<br/>' : ''

    message += key + ': '

    value.forEach((error, i2) => {
      message += (ERROR_CODES[error] || error) + (i2 + 1 !== value.length ? ', ' : '')
    })
  })

  return message
}

const ERROR_CODES = {
  'validation.unique': 'Значение не уникальное'
}

const STATUS_ERRORS = {
  400: 'Плохой, неверный запрос',
  401: 'Не авторизован',
  402: 'Необходима оплата',
  403: 'Запрещено',
  404: 'Не найдено',
  405: 'Метод не поддерживается',
  406: 'Неприемлемо',
  407: 'Необходима аутентификация прокси',
  408: 'Истекло время ожидания',
  409: 'Конфликт',
  410: 'Удалён',
  411: 'Необходима длина',
  412: 'Условие ложно',
  413: 'Полезная нагрузка слишком велика',
  414: 'URI слишком длинный',
  415: 'Неподдерживаемый тип данных',
  416: 'Диапазон не достижим',
  417: 'Ожидание не удалось',
  418: 'Я — чайник',
  421: 'Запрос был перенаправлен на сервер, не способный дать ответ',
  422: 'Необрабатываемый экземпляр',
  423: 'Заблокировано',
  424: 'Невыполненная зависимость',
  426: 'Необходимо обновление',
  428: 'Необходимо предусловие',
  429: 'Слишком много запросов',
  431: 'Поля заголовка запроса слишком большие',
  444: 'Нестандартный код',
  449: 'Повторить с',
  451: 'Недоступно по юридическим причинам'
}
