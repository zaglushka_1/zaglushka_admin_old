// TODO: Добавить комментарии
const objectToFormData = (obj, form, namespace) => {
  let fd = form || new FormData()
  let formKey

  for (let property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (namespace) {
        formKey = namespace + '[' + property + ']'
      } else {
        formKey = property
      }

      if (obj[property] instanceof Date) {
        fd.append(formKey, obj[property].toISOString())
      } else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, formKey)
      } else {
        fd.append(formKey, obj[property])
      }
    }
  }

  return fd
}

export default objectToFormData
