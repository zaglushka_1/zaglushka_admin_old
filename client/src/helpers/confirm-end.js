const beforeLeave = (component, to, from, next) => {
  if (component.isChanged) {
    if (confirm('Уйти с этой страницы? \nВозможно, внесенные изменения не сохранятся.')) {
      next()
      window.removeEventListener('beforeunload', component.handlerBeforeUnload)
    } else {
      next(false)
    }
  } else {
    window.removeEventListener('beforeunload', component.handlerBeforeUnload)
    next()
  }
}

const beforeUnload = (component, event) => {
  console.log(component)
  console.log(event)
  if (component.isChanged) {
    const confirmationMessage = 'Уйти с этой страницы? \nВозможно, внесенные изменения не сохранятся.'

    event.returnValue = confirmationMessage
    return confirmationMessage
  }
}

export { beforeLeave, beforeUnload }
