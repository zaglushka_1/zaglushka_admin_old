import moment from 'moment'

const offset = (date) => moment(date).add(moment().utcOffset(), 'minutes')

const format = (date) => offset(date).format('DD.MM.YYYY HH:mm')
const fromNow = (date) => offset(date).fromNow()

export { format, fromNow }
