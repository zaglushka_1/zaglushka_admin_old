import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import VueQuillEditor from 'vue-quill-editor'
import VueAcl from 'vue-acl'
import VeeValidate, { Validator } from 'vee-validate'
import ru from 'vee-validate/dist/locale/ru'

import decodeHtmlEntities from '@/helpers/decode-html-entities'
import encodeHtmlEntities from '@/helpers/encode-html-entities'

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'

require('moment/locale/ru')

Validator.localize('ru', {
  messages: {
    ...ru.messages,
    unique: (n) => 'Поле ' + n + ' должно быть уникальным.',
    string: (n) => 'Поле ' + n + ' должно быть строкой.'
  }
})

Vue.use(VueResource)
Vue.use(VueQuillEditor)
Vue.use(VeeValidate, {
  locale: 'ru'
})

Vue.use(VueAcl, {
  init: 'any',
  router: router,
  fail: '/not-found'
})

Vue.config.productionTip = false
Vue.config.errorHandler = err => {
  store.commit('pageload/error')

  store.commit('notification/add', {
    type: 'danger',
    text: 'Что-то пошло не так'
  })

  console.err(err)
}

sync(store, router)

Vue.filter('decodeHtmlEntities', decodeHtmlEntities)
Vue.filter('encodeHtmlEntities', encodeHtmlEntities)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
