<?php
Route::group(['middleware' => ['request_formatter'], 'prefix' => 'rin', 'namespace' => 'Modules\Rin\Http\Controllers'], function()
{
    Route::post('/login', 'RinController@login');
    Route::post('/logout', 'RinController@logout');

    Route::group(['middleware' => ['rin_access']], function ()
    {
        Route::group(['prefix' => 'api'], function()
        {
            Route::group(['middleware' => ['admin']], function ()
            {
                // settings
                Route::get('/settings', 'Admin\SettingsController@getSettings');
                Route::put('/settings', 'Admin\SettingsController@addSettings');
                Route::post('/settings/{id}', 'Admin\SettingsController@editSettings');
                Route::delete('/settings/{id}', 'Admin\SettingsController@deleteSettings');
                Route::get('/settings/sales-emails', 'Admin\SettingsController@getSalesEmails');
                Route::put('/settings/sales-emails', 'Admin\SettingsController@setSalesEmails');

                // admin
                Route::get('/admin/users', 'Admin\UserController@getList');
                Route::get('/admin/users/{id}', 'Admin\UserController@get');
                Route::put('/admin/users/{id}', 'Admin\UserController@update');

                // Менеджеры
                Route::get('/sales/orders', 'Sales\OrderController@getList');
                Route::get('/sales/orders/{id}', 'Sales\OrderController@get');
                Route::get('/sales/orders/download-file/{id}', 'Sales\OrderController@downloadFile');

                // Активность
                Route::get('/activity/list', 'ActivityController@getList');
                Route::get('/activity/subject/{subject_id}/{object_id}', 'ActivityController@getBySubject');
                Route::get('/activity/user/{id}', 'ActivityController@getByUser');
            });

            Route::group(['middleware' => ['seo']], function ()
            {
                // SEO
                Route::get('/seo/slugs/{subjectId}/{objectId}', 'Seo\SlugController@getList');
                Route::post('/seo/slugs', 'Seo\SlugController@add');
                Route::put('/seo/slugs/{id}', 'Seo\SlugController@update');
                Route::delete('/seo/slugs/{id}', 'Seo\SlugController@delete');

                Route::get('/seo/meta', 'Seo\MetaController@get');
                Route::put('/seo/meta', 'Seo\MetaController@update');

                Route::get('/tags-top', 'TagController@getListTop');
                Route::get('/tags-under', 'TagController@getListUnder');
                Route::get('/tags-seo', 'TagController@getListSeo');
                Route::get('/tags/{id}', 'TagController@get');

                Route::get('/seo/categories', 'Seo\CategoryController@getList');
                Route::get('/seo/categories/{id}', 'Seo\CategoryController@get');
                Route::put('/seo/categories/{id}', 'Seo\CategoryController@update');
            });

            Route::group(['middleware' => ['content']], function ()
            {
                // Content
                Route::get('/content/categories', 'Content\CategoryController@getList');
                Route::get('/content/categories/{id}', 'Content\CategoryController@get');
                Route::delete('/content/categories/{id}/slug', 'Content\CategoryController@deleteSlug');
                Route::post('/content/categories/{id}/slug', 'Content\CategoryController@createSlug');
                Route::post('/content/categories', 'Content\CategoryController@add');
                Route::put('/content/categories/{id}', 'Content\CategoryController@update');

                Route::get('/content/items/list/{subcategoryId}', 'Content\ItemController@getList');
                Route::get('/content/items/new', 'Content\ItemController@get');
                Route::get('/content/items/{id}', 'Content\ItemController@get');
                Route::delete('/content/items/{id}/slug', 'Content\ItemController@deleteSlug');
                Route::post('/content/items/{id}/slug', 'Content\ItemController@createSlug');
                Route::post('/content/items', 'Content\ItemController@create');
                Route::post('/content/items/copy/{id}', 'Content\ItemController@copy');
               
                Route::put('/content/items/image-multi-change', 'Content\ItemController@imageMultiChange');

                Route::put('/content/items/{id}', 'Content\ItemController@update');
                Route::post('/content/items/{id}', 'Content\ItemController@validate');
                Route::post('/content/items/{id}/image-swap', 'Content\ItemController@imageSwap');

                Route::get('/content/tags-top', 'Content\TagController@getListTop');
                Route::get('/content/tags-under', 'Content\TagController@getListUnder');
                Route::get('/content/tags-seotags', 'Content\TagController@getListSeo');
                Route::get('/content/tags/{id}', 'Content\TagController@get');
                Route::post('/content/tags', 'Content\TagController@add');
                Route::put('/content/tags/{id}', 'Content\TagController@update');
                Route::post('/content/tags/order', 'Content\TagController@order');

                Route::get('/content/props/{type}', 'Content\PropertyController@getList');
                Route::post('/content/props', 'Content\PropertyController@add');
                Route::put('/content/props/{id}', 'Content\PropertyController@update');
                Route::delete('/content/props/{id}', 'Content\PropertyController@delete');

                Route::get('/content/cities', 'Content\CityController@getList');
                Route::post('/content/cities', 'Content\CityController@add');
                Route::put('/content/cities/{id}', 'Content\CityController@update');
                Route::delete('/content/cities/{id}', 'Content\CityController@delete');

                Route::get('/content/managers', 'Content\ManagerController@getList');
                Route::post('/content/managers', 'Content\ManagerController@add');
                Route::put('/content/managers/{id}', 'Content\ManagerController@update');
                Route::delete('/content/managers/{id}', 'Content\ManagerController@delete');

                Route::get('/content/items-hot', 'Content\ItemHotController@getList');
                Route::get('/content/items-hot/{id}', 'Content\ItemHotController@get');
                Route::post('/content/items-hot', 'Content\ItemHotController@add');
                Route::put('/content/items-hot/{id}', 'Content\ItemHotController@update');
                Route::post('/content/items-hot/{id}', 'Content\ItemHotController@validate');
                Route::delete('/content/items-hot/{id}', 'Content\ItemHotController@delete');

                Route::post('/content/import', 'Content\ImportController@import');

                Route::get('/content/breadcrumbs', 'Content\BreadcrumbsController@getList');
                Route::get('/content/breadcrumbs/{categoryId}', 'Content\BreadcrumbsController@get');
                Route::put('/content/breadcrumbs/{categoryId}/{tagId?}', 'Content\BreadcrumbsController@update');

                Route::get('/content/complex', 'Content\ComplexController@getList');
                Route::get('/content/complex/{id}', 'Content\ComplexController@get');
                Route::put('/content/complex/{id}', 'Content\ComplexController@update');

                Route::get('/content/colors', 'Content\ColorController@getList');
                Route::put('/content/colors', 'Content\ColorController@update');
                Route::get('/content/colors/{id}', 'Content\ColorController@get');

                Route::get('/content/manufacturers', 'Content\ManufacturerController@getList');
                Route::put('/content/manufacturers', 'Content\ManufacturerController@update');

                Route::get('/content/undertitles', 'Content\UndertitlesController@getList');
                Route::put('/content/undertitles', 'Content\UndertitlesController@update');

                Route::get('/content/i10n', 'Content\I10nController@get');
                Route::put('/content/i10n/{id}', 'Content\I10nController@update');
            });

            Route::group(['middleware' => ['manager']], function ()
            {
            });

            // common
            Route::get('/user', 'UserController@show');

            Route::get('/search/items', 'SearchController@items');
            Route::get('/search/tags', 'SearchController@tags');
            Route::get('/search/categories', 'SearchController@categories');
            Route::get('/search/parent-categories', 'SearchController@parentCategories');

            Route::get('/items/list/{subcategoryId}', 'ItemController@getList');
            Route::get('/items/{id}', 'ItemController@get');

            Route::post('/exceptions', 'ExceptionController@set');
        });
    });

    // этот роут должен быть в самом низу
    Route::any('/{url?}', 'RinController@index')->where(['url' => '.*']);
});
