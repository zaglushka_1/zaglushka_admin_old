<?php

namespace Modules\Rin\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class IsHaveRinAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isHaveRinAccess()) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Not allowed',
        ], 403);
    }
}
