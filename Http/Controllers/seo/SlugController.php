<?php

namespace Modules\Rin\Http\Controllers\Seo;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Slug as SlugModel;
use Slug;
use Validator;

class SlugController extends BaseController
{
    /**
     * @param $subjectId
     * @param $objectId
     *
     * @return SlugModel[]
     */
    public function getList($subjectId, $objectId)
    {

        return SlugModel::where(['subject_id' => $subjectId, 'object_id' => $objectId])->get();
    }

    /**
     * @param Request $Request
     *
     * @return SlugModel
     */
    public function add(Request $Request)
    {
        Validator::make($Request->all(), [
            'slug'       => 'unique:slugs',
            'subject_id' => 'required',
            'object_id'  => 'required',
        ])->validate();

        $Slug          = new SlugModel($Request->all());
        $Slug->current = true;
        $Slug->slug    = str_slug(Slug::make($Slug->slug));

        $Slug->saveOrFail();

        SlugModel::where(['current'    => 1,
                     'subject_id' => $Request->get('subject_id'),
                     'object_id'  => $Request->get('object_id'),
        ])
            ->where('id', '!=', $Slug->id)
            ->update(['current' => 0]);

        return $Slug;
    }

    /**
     * Set current
     *
     * @param $id
     *
     * @return \App\Models\Slug
     */
    public function update($id)
    {
        /** @var SlugModel $Slug */
        $Slug = SlugModel::findOrFail($id);

        $Slug->current = true;

        $Slug->saveOrFail();

        SlugModel::where(['current' => 1, 'subject_id' => $Slug->subject_id, 'object_id' => $Slug->object_id])
            ->where('id', '!=', $Slug->id)
            ->update(['current' => 0]);

        return $Slug;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete($id)
    {
        /** @var SlugModel $Slug */
        $Slug = SlugModel::findOrFail($id);

        if ($Slug->current) {

            return false;
        }

        $Slug->delete();

        return 'ok';
    }
}
