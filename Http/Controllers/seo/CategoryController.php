<?php

namespace Modules\Rin\Http\Controllers\Seo;

use Modules\Rin\Models\Category;
use Illuminate\Http\Request;
use Modules\Rin\Http\Controllers\CategoryController as BaseCategoryController;

class CategoryController extends BaseCategoryController
{

    /**
     * @param int     $id
     * @param Request $Request
     *
     * @return Category
     */
    public function update($id, Request $Request)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($id);

        $Category->description_acceptable_for_subdomains = $Request->get('description_acceptable_for_subdomains');

        $Category->saveOrFail();

        $Category = $Category->fresh();

        return $Category;
    }
}
