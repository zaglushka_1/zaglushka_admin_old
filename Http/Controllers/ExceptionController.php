<?php

namespace Modules\Rin\Http\Controllers;

use Exception;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class ExceptionController extends BaseController
{
    /**
     * @param \Request $Request
     *
     * @throws \Exception
     * @internal param int $id
     */
    public function set(Request $Request)
    {
        throw new Exception($Request->get('exception'));
    }
}
