<?php

namespace Modules\Rin\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\Slug as ModelSlug;

class CategoryController extends BaseController
{

    /**
     * @return Category[]
     */
    public function getList()
    {
        $TopCategories = Category::where(['parent_id' => null])
            ->orderBy('ord', 'DESC')->get();

        foreach ($TopCategories as $Category) {
            $Category['subcategories'] = Category::where(['parent_id' => $Category->id])
                ->orderBy('ord', 'DESC')->get();

            foreach ($Category['subcategories'] as $Category) {
                $ModelSlug = ModelSlug::where([
                    'object_id'  => $Category->id,
                    'subject_id' => Subjects::SUBJECT_CATEGORY,
                    'current'    => true,
                ])->first();

                $Category->slug = $ModelSlug ? $ModelSlug->slug : null;
            }

        }
        return $TopCategories;
    }

    /**
     * @param int $id
     *
     * @return Category
     */
    public function get($id)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($id);

        $Category->images = $Category->getImagesWithUrl();

        return $Category;
    }

}
