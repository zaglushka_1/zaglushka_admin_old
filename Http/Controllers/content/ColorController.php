<?php

namespace Modules\Rin\Http\Controllers\Content;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Color;
use Validator;

class ColorController extends BaseController
{
    /**
     * @return Color[]
     */
    public function getList()
    {
        return Color::all();
    }

    /**
     * @param $id
     *
     * @return Color
     */
    public function get($id)
    {
        /** @var Color $Color */
        $Color = Color::findOrFail($id);

        return $Color;
    }

    /**
     * @param Request $Request
     *
     * @return Color[]
     */
    public function update(Request $Request)
    {
        if ($Request->get('added')) {
            foreach ($Request->get('added') as $added) {
                Validator::make($added, [
                    'name' => 'required',
                    'hex' => 'required',
                ])->validate();

                /** @var Color $Color */
                $Color = new Color($added);

                $Color->saveOrFail();
            }
        }

        if ($Request->get('updated')) {
            foreach ($Request->get('updated') as $updated) {
                /** @var Color $Color */
                $Color = Color::findOrFail($updated['id']);

                $Color->fill($updated);

                $Color->saveOrFail();
            }
        }

        return $this->getList();
    }
}
