<?php

namespace Modules\Rin\Http\Controllers\Content;

use Cache;
use File;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Category;
use Illuminate\Http\Request;
use Modules\Rin\Http\Controllers\CategoryController as BaseCategoryController;
use Modules\Rin\Models\Item;
use Modules\Rin\Models\Slug as ModelSlug;
use Slug;

class CategoryController extends BaseCategoryController
{
    /**
     * @param int $id
     *
     * @return Category
     */
    public function get($id)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($id);

        $Category->images      = $Category->getImagesWithUrl();
        $Category->items_count = $Category->items()->where(['enabled' => 1])->count();

        return $Category;
    }

    /**
     * @param Request $Request
     *
     * @return bool
     */
    public function add(Request $Request)
    {
        $Category = new Category();

        $Category->fill($Request->all());

        $Category->saveOrFail();

        if (isset($Request->allFiles()['images']['replaced'])) {
            foreach ($Request->allFiles()['images']['replaced'] as $key => $replaced) {
                /** @var \Illuminate\Http\UploadedFile $replaced */
                $replaced->move(public_path($Category->getImageFolder()), $Category->id . $key);
            }
        }

        $Slug = new ModelSlug();

        $Slug->subject_id = \App\Enum\Subjects::SUBJECT_CATEGORY;
        $Slug->object_id  = $Category->id;

        $Slug->slug    = str_slug(Slug::make($Category->title));
        $Slug->current = true;

        $Slug->saveOrFail();
    }

    /**
     * @param int     $id
     * @param Request $Request
     *
     * @return Category
     */
    public function update($id, Request $Request)
    {
        /*        Validator::make($Request->all(), [
                    'parent_id',
                    'title',
                    'ord',
                    'descr',
                    'type',
                    'sort',
                    'filter_title',
                    'filter_descr',
                    'filter_hide',
                    'enabled',
                    'title_h1',
                    'meta_title',
                    'meta_description',
                    'meta_keywords',
                    'title_h1_json',
                    'meta_title_json',
                    'meta_description_json',
                    'meta_keywords_json',
                ])->validate();*/

        /** @var Category $Category */
        $Category = Category::findOrFail($id);

        $Category->fill($Request->all());

        $Category->saveOrFail();

        if (isset($Request->get('images')['deleted'])) {
            foreach ($Request->get('images')['deleted'] as $deleted) {
                File::delete(public_path($Category->getImagePath($deleted)));
            }
        }

        if (isset($Request->allFiles()['images']['replaced'])) {
            foreach ($Request->allFiles()['images']['replaced'] as $key => $replaced) {
                /** @var \Illuminate\Http\UploadedFile $replaced */
                $replaced->move(public_path($Category->getImageFolder()), $Category->id . $key);
            }
        }

        if (!$Category->enabled) {
            $Items = Item::where(['cat_id' => $Category->id, 'enabled' => 1])->get();

            foreach ($Items as $Item) {
                $Item->enabled = false;
                $Item->save();
            }
        }

        $Category = $Category->fresh();

        return $Category;
    }

    public function deleteSlug($id)
    {
        $ModelSlugs = ModelSlug::where([
            'object_id'  => $id,
            'subject_id' => Subjects::SUBJECT_CATEGORY,
        ])->get();

        foreach ($ModelSlugs as $ModelSlug) {
            $ModelSlug->delete();
        }
    }

    public function createSlug($id)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($id);

        $Slug = ModelSlug::where([
            'subject_id' => Subjects::SUBJECT_CATEGORY,
            'object_id'  => $Category->id,
            'current'    => true,
        ])->first();

        if (!$Slug) {
            $Slug = new ModelSlug();

            $Slug->subject_id = Subjects::SUBJECT_CATEGORY;
            $Slug->object_id  = $Category->id;

            $Slug->slug    = str_slug(Slug::make(html_entity_decode($Category->title)));
            $Slug->current = true;

            $Slug->saveOrFail();
        }
    }
}
