<?php

namespace Modules\Rin\Http\Controllers\Content;

use App\Models\I10n;
use Cache;
use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Modules\Rin\Models\Category;

class I10nController extends BaseController
{
    /**
     * @return array
     */
    public function get()
    {
        $categories = I10n::select('category')->distinct()->pluck('category');

        $result = [];

        $Date = new Carbon();
        $Date->subMonth(2);

        foreach ($categories as $category) {
            $I10ns = I10n::where(['category' => $category])
                ->where('updated_at', '>', $Date)
                ->orderBy('updated_at', 'desc')->get();

            if ($category == 'categories_desc' || $category == 'categories_title') {
                foreach ($I10ns as &$I10n) {
                    $Cat = Category::find($I10n->ru);
                    if ($Cat) {
                        $I10n->ru = $Cat->title;
                    }
                }
            }

            $result[] = [
                'name'    => $category,
                'counter' => 0, // todo
                'list'    => $I10ns,
            ];
        }

        return $result;
    }

    /**
     * @param   int                    $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return bool
     */
    public function update($id, Request $Request)
    {
        /** @var I10n $I10n */
        $I10n = I10n::findOrFail($id);

        Cache::delete('i10n_' . $I10n->category . '_' . $I10n->key);
        Cache::delete('i10n_all_' . $I10n->key);

        $I10n->en = $Request->get('en') ? trim($Request->get('en')) : $I10n->en;
        $I10n->it = $Request->get('it') ? trim($Request->get('it')) : $I10n->it;

        return (string) $I10n->saveOrFail();
    }
}
