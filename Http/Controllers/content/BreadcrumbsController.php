<?php

namespace Modules\Rin\Http\Controllers\Content;

use Modules\Rin\Http\Controllers\CategoryController;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\Tag;
use Illuminate\Http\Request;

class BreadcrumbsController extends CategoryController
{
    /**
     * @param $categoryId
     *
     * @return array
     */
    public function get($categoryId)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($categoryId);
        $Category->breadcrumbs_image = $Category->getBreadcrumbsImage();

        /** @var Tag[] $Tags */
        $Tags = $Category->getTopTags();

        foreach ($Tags as $Tag) {
            $Tag->breadcrumbs_image = $Tag->isBreadcrumbsImageExists($Category) ? $Tag->getBreadcrumbsImage($Category) : '';
        }

        return [
            'Category' => $Category,
            'Tags'     => $Tags,
        ];
    }

    /**
     *
     */
    public function update(Request $Request, $categoryId, $tagId = null)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($categoryId);

        $File = $Request->file('image');

        if ($tagId) {
            /** @var Tag $Tag */
            $Tag = Tag::findOrFail($tagId); // ->where(['top_level' => 1])

            $filename = $Category->id . '_' . $Tag->id . '.jpg';
            $path = '/images/breadcrumbs/tags/';
        } else {
            $filename = $Category->id . '.jpg';
            $path = '/images/breadcrumbs/categories/';
        }
        $File->move(public_path($path), $filename);

        return $path . $filename;
    }
}
