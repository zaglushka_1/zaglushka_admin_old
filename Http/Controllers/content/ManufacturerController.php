<?php

namespace Modules\Rin\Http\Controllers\Content;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Item;

class ManufacturerController extends BaseController
{

    /**
     * @return Object[]
     */
    public function getList()
    {
        $Productions = Item::selectRaw('production, count(production) as production_count')
            ->where('production', '!=', '')
            ->groupBy('production')
            ->orderBy('production_count', 'DESC')
            ->get();

        return $Productions;
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return Object
     */
    public function update(Request $Request)
    {
        $manufacturersOld = $Request->get('manufacturer_old');
        $manufacturersNew = trim($Request->get('manufacturer_new'));

        return Item::where(['production' => $manufacturersOld])
            ->update(['production' => $manufacturersNew]);
    }
}
