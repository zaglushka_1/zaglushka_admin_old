<?php

namespace Modules\Rin\Http\Controllers\Content;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Item;

class UndertitlesController extends BaseController
{

    /**
     * @return Object[]
     */
    public function getList()
    {
        $Productions = Item::selectRaw('under_title, under_title_color, count(under_title) as under_title_count')
            ->where('under_title', '!=', '')
            ->groupBy('under_title', 'under_title_color')
            ->orderBy('under_title_count', 'DESC')
            ->get();

        return $Productions;
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return Object
     */
    public function update(Request $Request)
    {
        $underTitleOld = $Request->get('under_title_old');
        $underTitleNew = trim($Request->get('under_title_new'));
var_dump($underTitleOld);
        $Items = Item::where([
            'under_title' => $underTitleOld,
        ]);

        $underColorOld = $Request->get('under_title_color_old') ?: null;
        $underColorNew = trim($Request->get('under_title_color_new'));

        if ($underColorOld === null) {
            $Items->whereNull('under_title_color');
        } else {
            $Items->where(['under_title_color' => $underColorOld]);
        }

        return $Items->update([
                'under_title' => $underTitleNew,
                'under_title_color' => $underColorNew,
            ]);
    }
}
