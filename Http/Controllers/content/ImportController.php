<?php

namespace Modules\Rin\Http\Controllers\Content;

use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Item;

class ImportController extends BaseController
{
    /**
     */
    public function import()
    {
        $json = file_get_contents('http://www.webplastic.ru/export/prices/?hash=b74aeece020168c89b32989e41ca13cb');

        $prices = json_decode($json, true);

        $found           = 0;
        $updated         = 0;
        $notFound        = 0;
        $updatedArticles = [];

        foreach ($prices as $price) {
            $Item = Item::where(['title' => $price['articul']])
                ->orWhere(['title' => str_replace(',', '.', $price['articul'])])
                ->orWhere(['title' => str_replace('.', ',', $price['articul'])])
                ->first();

            if ($Item) {
                $found++;

                $data = [
                    'price0' => $price['price0'],
                    'price1' => $price['price1'],
                    'price2' => $price['price2'],
                    'price3' => $price['price3'],
                    'price4' => $price['price4'],
                    'price5' => $price['price5'],
                ];

                $Item->fill($data);

                if ($Item->getDirty() && $Item->save()) {
                    $updated++;
                    $updatedArticles[] = $price['articul'];
                }
            } else {
                $notFound++;
            }
        }

        return [
            'found'           => $found,
            'updated'         => $updated,
            'notFound'        => $notFound,
            'updatedArticles' => $updatedArticles,
        ];
    }
}
