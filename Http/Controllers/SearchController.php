<?php

namespace Modules\Rin\Http\Controllers;

use DB;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Tag;
use Modules\Rin\Models\Slug as ModelSlug;

class SearchController extends BaseController
{
    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function items(Request $Request)
    {
        $article = $Request->get('article');
        $code    = $Request->get('code');

        $Items = Item::query();
        if ($article) {
            $Items = $Items->where(function ($Builder) use ($article): void {
                $Builder->where('title', 'like', str_replace(',', '.', $article) . '%')
                    ->orWhere('title', 'like', str_replace('.', ',', $article) . '%')
                    ->orWhereExists(function ($query) use ($article) {
                        $query->select(DB::raw(1))
                            ->from('items_colors')
                            ->whereRaw('items_colors.code LIKE \'' . str_replace(',', '.', $article) . '%\' AND items_colors.item_id = items.id');
                    })
                    ->orWhereExists(function ($query) use ($article) {
                        $query->select(DB::raw(1))
                            ->from('items_colors')
                            ->whereRaw('items_colors.code LIKE \'' . str_replace('.', ',', $article) . '%\' AND items_colors.item_id = items.id');
                    });
            });
        }

        if ($code) {
            $Items = $Items->where('code', 'LIKE', $code . '%');
        }

        $Items = $Items
            ->orderBy('enabled', 'desc')
            ->orderBy('title', 'asc')
            ->select(['id', 'code', 'title', 'enabled'])
            ->get();

        foreach ($Items as $Item) {
            $ModelSlug = ModelSlug::where([
                'object_id'  => $Item->id,
                'subject_id' => Subjects::SUBJECT_ITEM,
                'current'    => true,
            ])->first();

            $Item->slug = $ModelSlug ? $ModelSlug->slug : null;
        }

        return $Items;
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories(Request $Request)
    {
        $article = $Request->get('article');

        $Categories = Category::where('title', 'LIKE', $article . '%')
            ->orderBy('enabled', 'desc')
            ->orderBy('title', 'asc')
            ->select(['id', 'title', 'enabled'])
            ->get();

        foreach ($Categories as $Category) {
            $ModelSlug = ModelSlug::where([
                'object_id'  => $Category->id,
                'subject_id' => Subjects::SUBJECT_CATEGORY,
                'current'    => true,
            ])->first();

            $Category->slug = $ModelSlug ? $ModelSlug->slug : null;
        }

        return $Categories;
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function parentCategories(Request $Request)
    {
        $article = $Request->get('article');

        $Categories = Category::where('title', 'LIKE', $article . '%')
            ->whereNull('parent_id')
            ->orderBy('enabled', 'desc')
            ->orderBy('title', 'asc')
            ->select(['id', 'title', 'enabled'])
            ->get();

        return $Categories;
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tags(Request $Request)
    {
        $title = $Request->get('article');

        $Tags = Tag::where('title', 'LIKE', $title . '%')
            ->orderBy('title', 'asc')
            ->select(['id', 'title'])
            ->get();

        return $Tags;
    }
}