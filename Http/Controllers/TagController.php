<?php

namespace Modules\Rin\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Tag;

class TagController extends BaseController
{

    /**
     * @return Tag[]
     */
    public function getListTop()
    {
        $Tags = Tag::where(['top_level' => 1])
            ->orderBy('ord')
            ->get();

        return $Tags;
    }

    /**
     * @return Tag[]
     */
    public function getListUnder()
    {
        $Tags = Tag::where(['under_table'=>1])
            ->orderBy('ord')
            ->get();

        return $Tags;
    }

    /**
     * @return Tag[]
     */
    public function getListSeo() {
        $Tags = Tag::where(['seo' => 1])
            ->orderBy('ord')
            ->get();

        return $Tags;
        
    }

    /**
     * @param $id
     *
     * @return Tag
     */
    public function get($id)
    {
        /** @var Tag $Tag */
        $Tag = Tag::findOrFail($id);

        return $Tag;
    }
}
