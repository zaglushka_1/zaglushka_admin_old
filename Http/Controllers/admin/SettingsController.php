<?php

namespace Modules\Rin\Http\Controllers\Admin;

use App\Models\Settings;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class SettingsController extends BaseController
{
    /**
     *
     */
    public function setSalesEmails(Request $Request)
    {
        if ($Request->get('emails')) {
            Settings::where(['name' => 'sender_email'])->delete();

            foreach ($Request->get('emails') as $email) {
                $SalesEmail = new Settings([
                    'name' => 'sender_email',
                    'value' => $email,
                ]);

                $SalesEmail->save();
            }
        }
    }

    /**
     * @return array
     */
    public function getSalesEmails()
    {
        $SalesEmails = Settings::where(['name' => 'sender_email'])->get();

        return $SalesEmails;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        $SalesEmails = Settings::all();

        return $SalesEmails;
    }

    /**
     *
     */
    public function addSettings(Request $Request)
    {
        if ($Request->get('name') && $Request->get('value')) {
            $Setting = new Settings([
                'name' => $Request->get('name'),
                'value' => $Request->get('value'),
            ]);

            $Setting->save();

            return $Setting;
        }
    }

    /**
     *
     */
    public function editSettings($id, Request $Request)
    {
        $Setting = Settings::findOrFail($id);

        $Setting->value = $Request->get('value');

        $Setting->save();
    }

    /**
     *
     */
    public function deleteSettings($id)
    {
        $Setting = Settings::findOrFail($id);

        $Setting->delete();
    }
}
