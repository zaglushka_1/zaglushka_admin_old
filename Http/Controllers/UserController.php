<?php

namespace Modules\Rin\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Routing\Controller as BaseController;

class UserController extends BaseController
{
    use ResetsPasswords;

    /**
     * @return User
     */
    public function show()
    {
        /** @var User $User */
        $User = Auth::user();

        $User->getAllPermissions();

        return $User;
    }
}
